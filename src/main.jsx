import React, { Suspense } from 'react'
import ReactDOM from 'react-dom/client'

import './css/index.css'

const router = createBrowserRouter([
  {
    path: "/",
    element: <Index></Index>,
    errorElement: <ErrorPage/>,
  },
]);

const root = ReactDOM.createRoot(document.querySelector('#root'))
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Index from './pages/Index'
import ErrorPage from './pages/Error-Page';


root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
)
