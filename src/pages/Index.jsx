
import { Canvas } from '@react-three/fiber'
import Experience from '../components/Experience.jsx'
import { Html, useProgress } from '@react-three/drei'
import { Suspense } from 'react'
import SpecDescription from '../components/SpecDescription.jsx'

import '../css/index.css'
import Title from '../components/Title.jsx'

const Fallback = () => (
    <Html center>
      <div className="loading">Loading...</div>
    </Html>
)


export default function Index(props) 
{
    const titleFontSpecs = "Realised with"
    const listFramework = ["ReactJs", "ThreeJs", "NodeJs", "MongoDb"]
    const listFrameworkUrl = ["./Image/React-icon.png", "./Image/threejs.png", "./Image/NodeJS.png", "./Image/mongodb.png"]

    const titleDevOpsSpecs = "Maintained with"
    const listDevOps = ["Docker", "Kubernetes", "Terraform", "Google Cloud Engine"]
    const listDevOpsImage = ["./Image/docker.png", "./Image/kubernetes.png", "./Image/terraform.png", "./Image/gce.png"]

    return <>   
        <Canvas className='z1'
            camera={ 
                {
                    fov: 70,
                    near: 0.1,
                    far: 150,
                    position: [ 0, 50, 30 ]
                } 
            }
        >
            <color args={ [ '#030202' ] } attach="background" />
            <Suspense fallback={<Fallback />}>
                <Experience />
            </Suspense>
        </Canvas>
        <SpecDescription 
                    title={titleFontSpecs}
                    listFramework={listFramework}
                    listUrlLogo={listFrameworkUrl}
                    position="left"
                     />
        <SpecDescription 
                    title={titleDevOpsSpecs}
                    listFramework={listDevOps}
                    listUrlLogo={listDevOpsImage}
                    position="right"
                     />
        <Title></Title>
    </>
}