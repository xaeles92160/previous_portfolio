import { Html } from "@react-three/drei"
import "../css/SpecDescription.css"
import { useEffect } from "react";
import { useState } from "react";

import '../css/Title.css'
export default function Title() {

    return <>
        <div className="title-bar">
            <div className="title-bar-wrapper">
                <h1 className="type-anim">My Portforlio</h1>
            </div>
        </div>
    </>
}