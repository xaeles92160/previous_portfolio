import { Html } from "@react-three/drei"
import "../css/SpecDescription.css"
import { useEffect } from "react";
import { useState } from "react";

function SpecCompany(props)
{
    const [classes, setClasses] = useState("card-spec-body-row");

    let company = <div key={props.index} className={classes}>
            <div className="card-spec-body-row-img">
                <img src={props.img}></img>
            </div>
            <div className="card-spec-body-row-title">
                <p>{props.title}</p>
            </div>
        </div>

    useEffect(() => {
        setInterval(() => {
            setClasses("card-spec-body-row fade-up")
        },(props.index + 1 + props.multiplier) * 500)
    })
    return <>
        {company}
    </>
}

export default function SpecDescription(props)
{
    const title = props.title;
    const list = props.listFramework;
    const urlLogo = props.listUrlLogo;
    const position = props.position;

    const arrayListUrlLogo = list.map((e, i) => {return [i, e, urlLogo[i]];});

    const multiplier = (position === 'left') ? 5 : 8
    let listItems = arrayListUrlLogo.map((obj, i) =>
        <SpecCompany index={obj[0]} title={obj[1]} img={obj[2]} multiplier={multiplier} ></SpecCompany>
    );

    return <>
    {position == "left" &&
        <div className="card-spec left-card-spec">
    
            <div className="card-spec-title ">
                <h1 className="fade-in-first">{title}</h1>
            </div>
            <div className="card-spec-body">
                {listItems}
            </div>
        </div>
    } 
    {position == "right" &&
    <div className="card-spec right-card-spec">

        <div className="card-spec-title">
            <h1 className="fade-in-second">{title}</h1>
        </div>
        <div className="card-spec-body">
            {listItems}
        </div>
    </div>
    }
    </>
}