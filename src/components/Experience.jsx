import { OrbitControls, Sparkles, useGLTF, useTexture } from "@react-three/drei";
import { MeshBasicMaterial, Color, MeshStandardMaterial, BoxGeometry, Vector3 } from "three";
import { EffectComposer, Bloom, Noise, Glitch } from "@react-three/postprocessing";
import { BlendFunction } from "postprocessing";
import { Text } from "@react-three/drei";
import { Effect } from "postprocessing";
import { useRef } from "react";

export default function Experience(props) 
{
    const group = useRef()
    const {nodes} = useGLTF('./Mesh/room.glb')
    const bakedChairTexture = useTexture('./Texture/bakedChair.jpg')
    const bakedWallTexture = useTexture('./Texture/bakedWall.jpg')
    const bakedScreenTexture = useTexture('./Texture/bakedScreen.jpg')
    const bakedTableTexture = useTexture('./Texture/bakedTable.jpg')
    const greyColor = new Color( 0xFFFFFF );
    const center = new Vector3(nodes.Table.position.x, nodes.Table.position.y - 5, nodes.Table.position.z); 
    const textPosition = new Vector3(nodes.Chair.position.x, nodes.Chair.position.y + 5, nodes.Chair.position.z + 3);

    bakedChairTexture.flipY = false
    bakedWallTexture.flipY = false
    bakedScreenTexture.flipY = false
    bakedTableTexture.flipY = false

    return <group ref={group} {...props} dispose={null}>
                <OrbitControls enablePan={false}  enableZoom={true} 
                minAzimuthAngle={-Math.PI / 10}
                maxAzimuthAngle={Math.PI / 10}
                minZoom={0}
                maxZoom={1}
                minPolarAngle={Math.PI / 3}
                maxPolarAngle={Math.PI - Math.PI / 1.6} 
                target={center}
                />
                <EffectComposer>
                    <Noise premultiply blendFunction={BlendFunction.AVERAGE}></Noise>
                </EffectComposer>

                <mesh geometry={nodes.Mur2.geometry} position={nodes.Mur2.position} rotation={nodes.Mur2.rotation}>
                    <meshBasicMaterial map={bakedWallTexture}/>
                </mesh>
                <mesh geometry={nodes.Mur3.geometry} position={nodes.Mur3.position} rotation={nodes.Mur3.rotation}>
                    <meshBasicMaterial map={bakedWallTexture}/>
                </mesh>
                <mesh geometry={nodes.Mur4.geometry} position={nodes.Mur4.position} rotation={nodes.Mur4.rotation}>
                    <meshBasicMaterial map={bakedWallTexture}/>
                </mesh>
                <mesh geometry={nodes.Sol.geometry} position={nodes.Sol.position} rotation={nodes.Sol.rotation}>
                    <meshBasicMaterial map={bakedWallTexture}/>
                </mesh>
                <mesh geometry={nodes.Chair.geometry} position={nodes.Chair.position} rotation={nodes.Chair.rotation}>
                    <meshBasicMaterial map={bakedChairTexture}/>
                </mesh>
                <mesh geometry={nodes.Table.geometry} position={nodes.Table.position} rotation={nodes.Table.rotation}>
                    <meshBasicMaterial map={bakedTableTexture}/>
                </mesh>
                <mesh geometry={nodes.Keyboard.geometry} position={nodes.Keyboard.position} rotation={nodes.Keyboard.rotation}>
                    <meshBasicMaterial map={bakedTableTexture}/>
                </mesh>
                <mesh geometry={nodes.TapisSouris.geometry} position={nodes.TapisSouris.position} rotation={nodes.TapisSouris.rotation}>
                    <meshBasicMaterial map={bakedTableTexture}/>
                </mesh>
                <mesh geometry={nodes.Souris.geometry} position={nodes.Souris.position} rotation={nodes.Souris.rotation}>
                    <meshBasicMaterial map={bakedTableTexture}/>
                </mesh>
                <mesh geometry={nodes.Screen.geometry} position={nodes.Screen.position} rotation={nodes.Screen.rotation}>
                    <meshBasicMaterial map={bakedScreenTexture}/>
                </mesh>
                <mesh geometry={nodes.Screen1.geometry} position={nodes.Screen1.position} rotation={nodes.Screen1.rotation}>
                    <meshBasicMaterial map={bakedScreenTexture}/>
                </mesh>
                <mesh geometry={nodes.Screen2.geometry} position={nodes.Screen2.position} rotation={nodes.Screen2.rotation}>
                    <meshBasicMaterial map={bakedScreenTexture}/>
                </mesh>
                <Sparkles speed={2} size={7} scale={[100, 90, 20]} position-z={-50} position-y={35} count={200} color={greyColor} noise={[0, 0, 1]}></Sparkles>

                <Text font="./font/Cyberpunk-Regular.ttf" position={textPosition} maxWidth={1} textAlign={center} color="salmon" fontSize={2}>Charles Leblanc</Text>
            
        </group>
}

useGLTF.preload('./Mesh/room.glb')